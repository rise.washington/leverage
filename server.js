const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require('dotenv').config()
const app = express();
const port = process.env.PORT;
const userDetails = require("./userDetails.json");
const viewRoutes = require("./routes/viewRoutes");
const apiRoutes = require("./routes/apiRoutes");

const aLoggerMiddleware = (req, res, next) => {
    console.log(res.method, req.url);
    next();
};

app.use(aLoggerMiddleware);
app.use(
  bodyParser.urlencoded({ extended: false}),
bodyParser.json({ extended: false})
);

app.use(express.static("public"), express.static("dist"));
app.use(viewRoutes);
app.use("/api", apiRoutes);

// app.get("/api/userBalance", (req, res) => {
//     res.json(userDetails.info);
// });

async function main() {
  await mongoose.connect("mongodb://localhost:27017/goose");

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
  });
}

main().catch((err) => console.error("er"));


