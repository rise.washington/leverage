const MDCTopAppBar = mdc.topAppBar.MDCTopAppBar;
const topAppBarElement = document.querySelector(".mdc-top-app-bar");
const topAppBar = new MDCTopAppBar(topAppBarElement);

const MDCTextField = mdc.textField.MDCTextElement;
const foos = [].map.call(
    document.querySelectorAll(".mdc-text-feild"),
    function (el) {
        return new MDCTextField(el);
    }
);   