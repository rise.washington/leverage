const model = {
  finnhubToken: "cdc4deqad3i6ap45jndgcdc4deqad3i6ap45jne0",
  isSearchingCryptos: false,
  userData: Json.parse(localStorgae.getItem("userData")) || {
    assets: {
      GOOG: {symbol: "GOOG", share: 1, type: "stock", price: 100},
    APPL: {symbol: "APPL", share: 1, type: "stock", price: 100},
    AMZN: {symbol: "AMZN", share: 1, type: "stock", price: 100},
    BTCUSDT: {symbol: "BTCUSDT", share: 1, type: "crypto", price: 100},
    },
    info: {balance: 10000, 
    },
  },
  timerinterval : null,
  timeLeft: 15,
  setTimeLeft(time) {
    model.timeLeft = time;
    view.updateTimer();
  },
};

const view = {
  refreshPage(stockInFocus) {
    focusTicker(stockInFocus);
    setStorage();
    view.refreshAssetList();
    view.intializeMaterialList();
  },
  updateElementAssetPrice(element, stockSymbol) {
    controller.fetchStockPrice(stockSymbol, function (stockQuote) {
      element.innerText = view.displayDollars(stockQuote.c);
    });
  },
  updateElementAssetValuation(
    element,
    stockSymbol,
    numShares,
    isPrefixed = true
  ){
    controller.fetchStockPrice(stockSymbol, function (stockQuote) {
      const prefix = isPrefixed ? `${numShares} shares:` : "";
      const displayText = prefix + view.displayDollars(stockQuote.c * numShares);
      element.innerText = displayText;
    });
  },
  refreshAssetList() {
    view.removeChildren(stockList);
    for (asset in model.userData.asset) {
      view.createAssetListItem(userData.assets[asset]);
    }
  },
  displayDollars(number) {
    return `$${number.toFixed(2)}`;
  },
  removeChildren(domNode) {
    while (domNode.firstChild) {
      domNode.removeChild(domNode.firstChild);
    }
  },
  makeMaterialButton(buttonAction) {
    const newButton = document.createElement("button");
    newButton.classList.add(
      "mdc-button",
      "mdc-card__action",
      "mdc-card__action--button"
    );
  
    const rippleDiv = document.createElement("div");
    rippleDiv.classList.add("mdc-button__ripple");
    newButton.appendChild(rippleDiv);
  
    const labelSpan = document.createElement("span");
    labelSpan.classList.add("mdc-button__label");
    labelSpan.innerText = buttonAction;
    newButton.appendChild(labelSpan);
  
    return newButton;
  },
  displaySearchError(errorMessage) {
    searchHelperText.innerText = errorMessage;
    searchHelperText.classList.add(
      "mdc-text-field-helper-text--validation-msg"
    );
    searchHelperText.classList.remove("mdc-text-field-helper-text");
  },
  hideSearchError() {
    searchHelperText.innerText = "";
    searchHelperText.classList.remove(
      "mdc-text-field-helper-text--validation-msg"
    );
    searchHelperText.classList.add("mdc-text-field-helper-text");
  },
  createAssetListItem(asset) {
    const newListItem = document.createElement("li");
    newListItem.classList.add("mdc-list-item", "space-between");
    newListItem.setAttribute("role", "option");
    newListItem.setAttribute("tabindex", "0");
    newListItem.setAttribute("data-ticker", asset.symbol);
  
    const rippleSpan = document.createElement("span");
    rippleSpan.classList.add("mdc-list-item__ripple");
    newListItem.appendChild(rippleSpan);
  
    const textSpan = document.createElement("span");
    textSpan.classList.add("mdc-list-item__text");
    textSpan.innerText = asset.symbol;
    newListItem.appendChild(textSpan);
  
    const usersShares = asset.shares;
    const sharesSpan = document.createElement("span");
    if (usersShares) {
      sharesSpan.innerText = "Loading ... ";
      updateElementAssetValuation(sharesSpan, asset.symbol, userData);
    } else {
      sharesSpan.innerText = "just watching";
    }
  
    newListItem.appendChild(sharesSpan);
    const priceSpan = document.createElement("span");
    updateElementAssetprice(priceSpan, asset.symbol);
    stockList.appendChild(newListItem);
  },
  async initializePage() {
    controller.focusTicker(Object.values(model.userData.assets)[0]);
    view.refreshAssetList();
    view.initializeHelperText();

    try {
      const fetchBalanceResponse = await fetch("/api/userBalance");
      const fetchBalanceData = await fetchBalanceResponse.json();
      model.updateBalance(fetchBalanceData.balance);
    }
    catch(e) {
      console.error("error fetching balance", e);
    }
  },
  intializeMaterialList() {
    const MDCList = mdc.list.MDCList;
    const MDCRipple = mdc.ripple.MDCRipple;
    const list = new MDCList(document.querySelector(".mdc-list"));
    const listItemRipples = list.listElements.map(
      (listItemEl) => new MDCRipple(listItemEl)
    );
  },
  initializeHelperText() {
    const MDCTextFieldHelperText = mdc.textField.MDCTextFieldHelperText;
    const helperText = new MDCTextFieldHelperText(
      document.querySelector(".mdc-text-field-helper-text")
    );
  },
  updatTimer() {
    timerSpan.innerText = model.timeLeft;
  },
};


const controller = {
  buyStock(stock) {
    if (model.userData.assets[stock].shares) {
      model.userData.assets[stock].shares += 1;
    } else {
      model.userData.assets[stock].shares = 1;
    }
    view.refreshPage(stock);
  },
  sellStock(stock) {
    model.userData.assets[stock].shares -= 1;
    view.refreshPage(stock);
  },
  watchStock(stock) {
    model.userData.assets[stock].shares = 0;
    view.refreshPage(stock);
  },
  ignoreStock(stock) {
    if (model.userData.assets[stock].shares) {
      alert(" it is prudent to sell before you ignore! ");
    } else {
      delete model.userData.assets[stock];
    }
    view.refreshPage(stock);
  },
  fetchStockPrice(stockSymbol, callback) {
    if (!stockSymbol) return;
    fetch(
      `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${model.finnhubToken}`
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.c == 0) throw `Oh no! Probably ${stockSymbol} is not a stock`;
        callback(data);
        view.hideSearchError();
      })
      .catch((thrownError) => view.displaySearchError(thrownError));
  },
  fetchCryptoPrice(crytpoSymbol, callback) {
    const baseUrl = "https://api.binance.com";
    fetch(baseUrl + `/api/v3/ticker/price?symbol=${crytpoSymbol}`)
      .then((data) => data.json())
      .then((data) => {
        callback(data);
        view.hideSearchError();
      })
      .catch((_) =>
        view.displaySearchError(`Sure ${crytpoSymbol} is a crypto?`)
      );
  },
  fetchAssetPrice(assetSymbol) {
    model.isSearchingCryptos
      ? controller.fetchCryptoPrice(assetSymbol)
      : controller.fetchStockPrice(assetSymbol);
  },
  toggleSearchMode() {
    focusHeadline.innerText = model.isSearchingCryptos
      ? "Searching Stocks"
      : "Searching Cryptos";
    toggleButton.childNodes[3].innerText = model.isSearchingCryptos
      ? "Search Cryptos"
      : "Search Stocks";
    model.isSearchingCryptos = !model.isSearchingCryptos;
  },
  searchTicker() {
    const searchedTicker = tickerSearchInput.value;
    if (controller.validateTickerInput(searchedTicker)) {
      controller.fetchAssetPrice(searchedTicker, console.log);
    } else {
      view.displaySearchError("you are searching for nothing");
    }
  },
  validateTickerInput(tickerSymbol) {
    return tickerSymbol !== "";
  },
  focusTicker(symbol) {
    view.removeChildren(focusedDescription);
    view.removeChildren(focusedCTAs);

    const stockHeadlines = document.createElement("p");
    stockHeadlines.classList.add(
      "flex-center",
      "space-between",
      "ampler-padding",
      "half-width"
    );
    const stockSymbol = document.createElement("span");
    stockSymbol.innerText = symbol;
    const stockPrice = document.createElement("span");
    stockPrice.innerText = "Loading";
    view.updateElementAssetPrice(stockPrice, symbol);
    stockHeadlines.append(stockSymbol, stockPrice);
    focusedDescription.appendChild(stockHeadlines);

    const usersAsset = model.userData.assets[symbol];
    const usersShares = usersAsset?.shares;

    const buyButton = view.makeMaterialButton("Buy");
    buyButton.addEventListener("click", function () {
      controller.buyStock(symbol);
    });
    focusedCTAs.appendChild(buyButton);

    if (usersShares) {
      const userStake = document.createElement("p");
      userStake.classList.add(
        "flex-center",
        "space-between",
        "ampler-padding",
        "half-width"
      );
      const numberOfShares = document.createElement("span");
      numberOfShares.innerText = `${usersShares} shares`;
      const valuation = document.createElement("span");
      valuation.innerText = "Loading...";
      view.updateElementAssetValuation(valuation, symbol, usersShares, false);
      userStake.append(numberOfShares, valuation);
      focusedDescription.appendChild(userStake);
      const sellButton = view.makeMaterialButton("Sell");
      sellButton.addEventListener("click", () => {
        controller.sellStock(symbol);
      });
      focusedCTAs.appendChild(sellButton);
    }

    if (usersAsset) {
      //this is equivalent to the fact that it is being watched
      const ignoreButton = view.makeMaterialButton("Ignore");
      ignoreButton.addEventListener("click", function () {
        controller.ignoreStock(symbol);
      });
      focusedCTAs.appendChild(ignoreButton);
    } else {
      const watchButton = view.makeMaterialButton("Watch");
      watchButton.addEventListener("click", function () {
        controller.watchStock(symbol);
      });
      focusedCTAs.appendChild(watchButton);
    }

    const stockDescription = document.createElement("p");
    stockDescription.innerText = `This lorem concerning ${symbol} ipsum dolor sit amet consectetur adipisicing elit. Tempore, at ullam repellendusexpedita aperiam optio, rem quos voluptate ea facere velit cumcommodi placeat nesciunt deserunt quidem. Aspernatur,repellendus nobis?`;
    focusedDescription.appendChild(stockDescription);

    tickerSearchInput.value = symbol;
  },
  focusListItem(event) {
    let nodeOfInterest = event.target;
    if (nodeOfInterest.matches("ul")) return;
    while (!nodeOfInterest.matches("li")) {
      nodeOfInterest = nodeOfInterest.parentNode;
    }
    const dataAttribute = nodeOfInterest.dataset["ticker"];
    controller.focusTicker(dataAttribute);
  },
  setStorage() {
    localStorage.setItem("userData", JSON.stringify(model.userData));
  },
  startTimer() {
    model.setTimeLeft(15);
    model.timerinterval = setInterval(() => {
      if (model.timeShown == 0) {
        // return clearInterval(clockInterval);
        return controller.timeoutTrade(model.timerinterval)
        // tradeDialog.close();
      }
      model.setTimeLeft(model.timeLeft -1);
    }, 1000);
  },
  timeoutTrade() {
    clearInterval(model.timerinterval);
    tradeDialog.close();
  },
  async fetchCryptoprice(cryptoSymbol) {
    try {
      const baseUrl = "https://api.binance.com";
      const res = await fetch(baseUrl + `/api/v3/ticker/price?symbol=${cryptoSymbol}`)
      const data = await res.json();
      view.hideSearchError();
      return {
        price: data.price,
        symbol: cryptoSymbol,
        type: "Crypto"
      }
    } catch(_) {
      view.displaySearchError(`Sure ${cryptoSymbol} is a cypto?`)
    }
  },
  async fetchStockPrice(stockSymbol) {
    try {
      if (!stockSymbol) return;
      const res = await fetch(
        `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${model.finnhubToken}`
      );
      const data = await res.json();

      if (data.c == 0) throw `oh no! Probably ${stockSymbol} is not a stock`;
      view.hideSearchError();
      return {
        price: parseFloat(parseFloat(data.price).toFixed(2)),
        symbol: cryptoSymbol,
        type: "crypt",
      };
    } catch (thrownError) {
      view.displaySearchError(thrownError);
    }
  },
};

const focusHeadline = document.querySelector("#focus-headline");
const focusedDescription = document.querySelector("#focused-description");
const focusedCTAs = document.querySelector("#focused-ctas");
const toggleButton = document.querySelector("#toggle-focus");
toggleButton.addEventListener("click", controller.toggleSearchMode);

const tickerSearchIcon = document.querySelector("#ticker-search");
const tickerSearchInput = document.querySelector("#ticker-input");
const searchHelperText = document.querySelector("#helper-text");
tickerSearchIcon.addEventListener("click", controller.searchTicker);
tickerSearchInput.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    tickerSearchIcon.click();
  }
});
const stockList = document.querySelector("ul#stock-list");
stockList.addEventListener("click", controller.focusListItem);

// const tradeDialog = new mdc.dialog.MDCDialog(
//   document.querySelector(".mdc-dialog")
// );

const tradeTitle = document.querySelector("#trade-title");
const confirmtradeButton = document.querySelector("#comfirm=trade-button");
confirmtradeButton.addEventListener("click", controller.confirmTrade);
const tradeSharesInput = document.querySelector("#trade-shares-input");
const timerSpan = document.querySelector("#trade-timer");
const alertDialog = new mdc.dialog.MDCDialog(document.querySelector("3alert-dialog"));
const alertContent = document.querySelector("#alert-content");
alertButton.addEventListener("click", view.dimissAlert);

view.initializePage();
// leave the socket stuff for next time
let socket;

function openBinanceWebsocket() {
  socket = new WebSocket("wss://stream.binance.com:9443/ws/btcusdt@miniTicker");

  socket.onopen = function (event) {
    console.log("Websocket connetion open", event);
  };

  socket.onmessage = function (event) {
    console.log(event);
    let data = JSON.parse(event.data);
    console.log(data);
    console.log(
      "current bitcoin price",
      view.displayDollars(parseFloat(data.c))
    );
  };
}

function subscribeBinanceSymbol(symbol) {
  socket.send(
    JSON.stringify({
      method: "SUBSCRIBE",
      params: [`${symbol}@miniTicker`],
      id: 1,
    })
  );
}
