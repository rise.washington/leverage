console.log(document.form);
const loginForm = document.forms[0];
const registationForm = document.forms[1];

const registerButton = registationForm[6];
function validateEligibilty(event){
    // event.preventDefault();
    if (noEmptyFields() && passwordsMatch() && legalCheckboxChecked()) {
        console.log("so valid");
    } else {
        console.log("not so valid");
    }
}

registerButton.addEventListener("click", validateEligibilty);

function isNotEmpty(input){
    return input.value !=="";
}

const usernameInput = registationForm[0];
const passwordInput = registationForm[1];
const confirmPasswordInput = registationForm[2];
const securityQuestionInput = registationForm[3];
const securityAnswerInput= registationForm[4];

const registationFormInputs = [
    usernameInput,
    passwordInput,
    confirmPasswordInput,
    securityQuestionInput,
    securityAnswerInput
];

function noEmptyFields(){
    return registationFormInputs.every(isNotEmpty);
}

function passwordsMatch() {
    return passwordInput.value === confirmPasswordInput.value
}

function legalCheckboxChecked() {
    return registationForm[5].checked;
}