const router = require("express").Router();
const path = require("path");

const serveLandingPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
};

router.get("/", serveLandingPage);

router.get("/about", serveLandingPage);

router.get("/portfolio", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/portfolio.html"));
});

module.exports = router;
