const { MongoClient } = require("mongodb");
let url = "mongodb://0.0.0:27017";
console.log(process.env.MONGO_PASSWORD);
if (process.env.MODE == "production") {
    url = `mongodb://2217135:${process.env.MONGO_PASSWORD}@192.168.171.67`;
}
const mongoClient = new MongoClient(url);
module.exports = mongoClient;