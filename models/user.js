const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    username: { type: String, require: true, unique: true },
    password: { type: String, require: true },
    securityQuestion: { type: String, require: true },
    securityAnswer: { type: String, require: true },
    balance: { type: Number, require: true },
    assets: [{ type: mongoose.Schema.Types.ObjectId, ref:
    "Asset" }],
});


const User = mongoose.model("User", userSchema);
module.exports = User;